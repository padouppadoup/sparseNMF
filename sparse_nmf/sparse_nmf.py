#-*-coding:utf-8-*-
## SPARSE_NMF Sparse NMF with beta-divergence reconstruction error, 
## L1 sparsity constraint, optimization in normalized basis vector space.
## =====================================================================
#
# References: 
# J. Eggert and E. Korner, "Sparse coding and NMF," 2004
# P. D. O'Grady and B. A. Pearlmutter, "Discovering Speech Phones 
#   Using Convolutive Non-negative Matrix Factorisation
#   with a Sparseness Constraint," 2008
# J. Le Roux, J. R. Hershey, F. Weninger, "Sparse NMF - half-baked or well 
#   done?," 2015
#
# This implementation follows the derivations in:
# If you use this code, please cite:
# J. Le Roux, J. R. Hershey, F. Weninger, 
# "Sparse NMF - half-baked or well done?," 
# MERL Technical Report, TR2015-023, March 2015
#   @TechRep{LeRoux2015mar,
#     author = {{Le Roux}, J. and Hershey, J. R. and Weninger, F.},
#     title = {Sparse {NMF} - half-baked or well done?},
#     institution = {Mitsubishi Electric Research Labs (MERL)},
#     number = {TR2015-023},
#     address = {Cambridge, MA, USA},
#     month = mar,
#     year = 2015
#   }
# Python version by Maxime Woringer, Jul. 2016
###########################################################################
#   Copyright (C) 2015 Mitsubishi Electric Research Labs (Jonathan Le Roux,
#                                         Felix Weninger, John R. Hershey)
#   Apache 2.0  (http://www.apache.org/licenses/LICENSE-2.0) 
###########################################################################

## ==== Importations
import time
import numpy as np


## ==== Main functions
def sparse_nmf(v, beta=1, cf='kl', sparsity=0, max_iter=100, conv_eps=0,
               display=False, random_seed=1, init_w=None, r=None, init_h=None,
               w_update_ind=None, h_update_ind=None):
    """Compute a sparse non-negative matrix factorization.

    Inputs:
    - v:  matrix to be factorized
    - beta:     beta-divergence parameter (default: 1, i.e., KL-divergence)
    - cf:       cost function type (default: 'kl'; overrides beta setting)
               'is': Itakura-Saito divergence
               'kl': Kullback-Leibler divergence
               'ed': Euclidean distance
     - sparsity: weight for the L1 sparsity penalty (default: 0)
     - max_iter: maximum number of iterations (default: 100)
     - conv_eps: threshold for early stopping (default: 0, i.e., no early stopping)
     - display:  display evolution of objective function (default: False)
     - random_seed: set the random seed to the given value 
                   (default: 1; if equal to 0, seed is not set)
     - init_w:   initial setting for W (default: random; either init_w or r have to be set)
     - r:  basis functions (default: based on init_w's size; either init_w or r have to be set)
     - init_h:   initial setting for H (default: random)
     - w_update_ind: set of dimensions to be updated (default: all)
     - h_update_ind: set of dimensions to be updated (default: all)

    Outputs:
    - w: matrix of basis functions
    - h: matrix of activations
    - objective: objective function values throughout the iterations
    """
    ## ==== Parsing inputs
    v = np.float_(v)
    m,n = v.shape
    if cf=='is': # overwrite cost function
        beta=0
    elif cf=='kl':
        beta=1
    elif cf=='ed':
        beta=2
        
    if random_seed>0:
        random_seed = int(random_seed)
        print('WARNING: using non-random seed for matrix optimisation: '+str(random_seed))
        np.random.seed(random_seed)
        
    if init_w is None: ## init w
        if r is None:
            raise InputError('Number of components or initialization must be given')
        w = np.random.rand(m,r)
    else:
        ri = init_w.shape[1]
        w[:,0:ri] = init_w ## to be tested
        if r is not None and ri < r:
            w[:, ri:r] = np.random.rand(m,r-ri) ## to be carefully checked
        else:
            r = ri
            
    if init_h is None: ## init h
        h = np.random.rand(r,n)
    elif init_h=='ones':
        print('INFO: initializing H with ones')
        h = np.ones((r,n))
    else:
        h =  init_h
    
    if w_update_ind is None:
        w_update_ind = np.ones((r,1)).astype(bool)
    if h_update_ind is None:
        h_update_ind = np.ones((r,1)).astype(bool)


    if type(sparsity) in (int, float): # sparsity per matrix entry
        sparsity = np.ones((r,n))*sparsity
    elif sparsity.shape[1] == 1:
        sparsity = np.tile(sparsity, (1,n)) ## to debug

    ## Normalize the columns of W and rescale H accordingly
    wn = ((w**2).sum(axis=0))**0.5          ##wn = sqrt(sum(w.^2));
    w = w/np.tile(wn, (w.shape[0], 1))      ##w  = bsxfun(@rdivide,w,wn);
    h = h*np.tile(wn, (h.shape[1], 1)).T    ##h  = bsxfun(@times,  h,wn');
    
    ## Internal parameters
    flr = 1e-9
    lamb = w.dot(h) # used to be lambda (but lambda is a reserved keyword in Python)
    lamb[lamb<flr]=flr 
    last_cost = np.inf

    objective_div = np.zeros((max_iter,))
    objective_cost = np.zeros((max_iter,))
    div_beta  = beta
    h_ind = h_update_ind.flatten()
    w_ind = w_update_ind.flatten()
    update_h = h_ind.sum(0)
    update_w = w_ind.sum(0)

    print('INFO: Performing sparse NMF with beta-divergence, beta={}'.format(div_beta))
    tic = time.time()

    ## ==== Main loop
    for it in range(max_iter):
        # ==== Updates H
        if update_h>0:
            if div_beta==1:
                dph = np.tile(w[:, h_ind].sum(0).reshape(1,-1), (sparsity.shape[1], 1)).T+sparsity
                dph[dph<flr]=flr
                dmh = np.dot(w[:, h_ind].T, v/lamb)
                h[h_ind,:] = h[h_ind,:]*dmh/dph
                #dph = bsxfun(@plus, sum(w(:, h_ind))', params.sparsity);
                #dph = max(dph, flr);
                #dmh = w(:, h_ind)' * (v ./ lamb);
                #h(h_ind, :) = bsxfun(@rdivide, h(h_ind, :) .* dmh, dph);
            elif div_beta==2:
                dph = np.dot(w[:, h_ind].T, lamb) +sparsity
                dph[dph<flr]=flr
                dmh = np.dot(w[:, h_ind].T, v)
                #dph = w(:, h_ind)' * lambda + params.sparsity;
                #dph = max(dph, flr);
                #dmh = w(:, h_ind)' * v;
            else:
                dph = np.dot(w[:, h_ind].T, lamb**(div_beta-1)) +sparsity
                dph[dph<flr]=flr
                dmh = np.dot(w[:, h_ind].T, v*lamb**(div_beta-2))
                h[h_ind, :] = h[h_ind,:]*dmh/dph
                #dph = w(:, h_ind)' * lambda.^(div_beta - 1) + params.sparsity;
                #dph = max(dph, flr);
                #dmh = w(:, h_ind)' * (v .* lamb.^(div_beta - 2));
                #h(h_ind, :) = h(h_ind, :) .* dmh ./ dph;
            lamb = np.dot(w,h)
            lamb[lamb<flr] = flr

        # ==== Updates W
        if update_w>0:
            if div_beta==1:
                dpw1 = np.tile(h[w_ind,:].sum(1).T, (w.shape[0],1))
                dpw2 = (np.dot(v/lamb, h[w_ind,:].T)* w[:,w_ind]).sum(0)
                dpw2 = np.tile(dpw2,  (w.shape[0],1))* w[:,w_ind]                
                dpw = dpw1 + dpw2
                dpw[dpw<flr]=flr
                #dpw = bsxfun(@plus,sum(h(w_ind, :), 2)', ...
                #    bsxfun(@times, sum((v ./ lambda) * h(w_ind, :)' .* w(:, w_ind)), w(:, w_ind)));
                #dpw = max(dpw, flr);
                dmw1 = np.dot(v/lamb, h[w_ind,:].T)
                dmw2 = (np.tile(h[w_ind,:].sum(1).T, (w.shape[0],1)) * w[:, w_ind]).sum(0)
                dmw2 = np.tile(dmw2,  (w.shape[0], 1))* w[:,w_ind]
                dmw = dmw1 + dmw2                
                #dmw = v ./ lamb * h(w_ind, :)' ...
                #    + bsxfun(@times, sum(bsxfun(@times, sum(h(w_ind, :),2).T, w(:, w_ind))), w(:, w_ind));
                w[:, w_ind] = w[:, w_ind] * dmw /dpw                
                #w(:, w_ind) = w(:,w_ind) .* dmw ./ dpw;                
            elif div_beta==2:
                dpw1 = np.dot(lamb , h[w_ind, :].T)
                dpw2 = (np.dot(v, h[w_ind,:].T)*w[:,w_ind]).sum(0)
                dpw2 = np.tile(dpw2, (w.shape[0], 1)) * w[:, w_ind]
                dpw = dpw1 + dpw2
                dpw[dpw<flr] = flr
                #dpw = lamb * h(w_ind, :)' ...
                #    + bsxfun(@times, sum(v * h(w_ind, :)' .* w(:, w_ind)), w(:, w_ind));
                #dpw = max(dpw, flr);
                dmw1 = np.dot(v, h[w_ind,:].T)
                dmw2 = (np.dot(lamb, h[w_ind,:].T)*w[:,w_ind]).sum(0)
                dmw2 = np.tile(dmw2, (w.shape[0], 1)) * w[:, w_ind]
                dmw = dmw1 + dmw2
                #dmw = v * h(w_ind, :)' + ...
                #    bsxfun(@times, sum(lambda * h(w_ind, :)' .* w(:, w_ind)), w(:, w_ind));
                w[:,w_ind] = w[:,w_ind] * dmw / dpw
                #w(:, w_ind) = w(:,w_ind) .* dmw ./ dpw;
            else:
                dpw1 = np.dot(lamb**(div_beta-1), h[w_ind, :].T)
                dpw2 = (np.dot(v*lamb**(div_beta-2), h[w_ind,:].T) * w[:, w_ind]).sum(0)
                dpw2 = np.tile(dpw2, (w.shape[0], 1)) * w[:, w_ind]
                dpw = dpw1 + dpw2
                dpw[dpw<flr] = flr
                #dpw = lamb.^(div_beta - 1) * h(w_ind, :)' ...
                #    + bsxfun(@times, ...
                #    sum((v .* lambda.^(div_beta - 2)) * h(w_ind, :)' .* w(:, w_ind)), ...
                #    w(:, w_ind));
                #dpw = max(dpw, flr);
                dmw1 = np.dot(v*lamb**(div_beta-2), h[w_ind, :].T)
                dmw2 = (np.dot(lamb**(div_beta-1), h[w_ind, :].T) * w[:, w_ind]).sum(0)
                dmw2 = np.tile(dmw2, (w.shape[0], 1)) * w[:, w_ind]
                dmw = dmw1 + dmw2
                w[:,w_ind] = w[:,w_ind] * dmw / dpw
                #dmw = (v .* lamb.^(div_beta - 2)) * h(w_ind, :)' ...
                #    + bsxfun(@times, ...
                #    sum(lambda.^(div_beta - 1) * h(w_ind, :)' .* w(:, w_ind)), w(:, w_ind));
                #w(:, w_ind) = w(:,w_ind) .* dmw ./ dpw;
            # Normalize the columns of W
            w = w/np.tile((w**2).sum(0)**0.5, (w.shape[0],1))
            lamb = np.dot(w, h)
            lamb[lamb<flr]=flr
            #w = bsxfun(@rdivide,w,sqrt(sum(w.^2)));

        # ==== Compute the objective function
        if div_beta == 1:
            div = (v*np.log(v/lamb)-v+lamb).sum()
        elif div_beta == 2:
            div = ((v-lamb)**2).sum()
        elif div_beta == 0:
            div = (v/lamb-log(v/lamb)-1).sum()
        else:
            div = (v**div_beta + (div_beta-1)*lamb**div_beta - div_beta*v*lamb**(div_beta-1)).sum() / (div_beta*(div_beta-1))
            #div = sum(sum(v.^div_beta + (div_beta - 1)*lamb.^div_beta - div_beta * v .* lamb.^(div_beta - 1))) / (div_beta * (div_beta - 1))
        cost = div + (sparsity*h).sum();

        objective_div[it] =  div
        objective_cost[it] =  cost
    
        # ==== Output if needed
        if display:
            print('iteration {}: div={}, cost={}'.format(it, div, cost))

        # ==== Convergence check
        if it > 0 and conv_eps>0:
            e = np.abs(cost - last_cost)/last_cost
            if e<conv_eps:
                print('Convergence reached, aborting iteration')
                objective_div = objective_div[:it] # truncate the output at the right size
                objective_cost = objective_cost[:it]
                break # exit the loop
        last_cost = cost
    toc = time.time()
    if display:
        print('Algorithm run in {}s'.format(toc-tic))
    last_cost = cost;
    return {'w': w, 'h': h, 'div': objective_div, 'cost': objective_cost}
