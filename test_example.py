#-*-coding:utf-8-*-
# Example script for Sparse NMF with beta-divergence distortion function
# and L1 penalty on the activations.
#
# If you use this code, please cite:
# J. Le Roux, J. R. Hershey, F. Weninger, 
# "Sparse NMF - half-baked or well done?," 
# MERL Technical Report, TR2015-023, March 2015
#   @TechRep{LeRoux2015mar,
#     author = {{Le Roux}, J. and Hershey, J. R. and Weninger, F.},
#     title = {Sparse {NMF} - half-baked or well done?},
#     institution = {Mitsubishi Electric Research Labs (MERL)},
#     number = {TR2015-023},
#     address = {Cambridge, MA, USA},
#     month = mar,
#     year = 2015
#   }
# Python version by Maxime W., Jul. 2016
###########################################################################
#   Copyright (C) 2015 Mitsubishi Electric Research Labs (Jonathan Le Roux,
#                                         Felix Weninger, John R. Hershey)
#   Apache 2.0  (http://www.apache.org/licenses/LICENSE-2.0) 
###########################################################################

## ==== importations
import scipy.io
import numpy as np
import sparse_nmf
import matplotlib.pyplot as plt

## ==== Input data
## You need to provide a non-negative matrix v to be factorized.
inp_data = scipy.io.loadmat('./dev/demodata/imagedata.mat')
v = np.hstack((inp_data['f'], inp_data['f'].T))
vv = np.hstack((inp_data['f'].T, inp_data['f']))
v = np.vstack((v,vv, vv, v))

print('running an input matrix of size {}'.format(v.shape))

## ==== Input parameters
## Objective function
cf = 'kl' # 'is', 'kl', 'ed'; takes precedence over setting the beta value. Alternately define: beta = 1
sparsity = 5

## Stopping criteria
max_iter = 100
conv_eps = 1e-3

display   = True # Display evolution of objective function
random_seed = 1 # Random seed: any value over than 0 sets the seed to that value

## Optional initial values for W 
# init_w # Number of components: if init_w is set and r larger than the number of basis functions in init_w, the extra columns are randomly generated
r = 1500 # This is basically the number of atoms
# init_h # Optional initial values for H: if not set, randomly generated 

## List of dimensions to update: if not set, update all dimensions.
#w_update_ind = np.ones((r,1)).astype(bool) #true(r,1) # set to false(r,1) for supervised NMF
#h_update_ind = np.ones((r,1)).astype(bool) #true(r,1)

res = sparse_nmf.sparse_nmf(v,
                            cf=cf,
                            sparsity=sparsity,
                            max_iter=max_iter,
                            conv_eps=conv_eps,
                            display=display,
                            random_seed=random_seed,
                            r=r);
w = res['w']
h = res['h']
objective_div = res['div']
objective_cost = res['cost']

plt.figure();plt.imshow(h, cmap='gray', interpolation='none')
plt.figure();plt.imshow(w, cmap='gray', interpolation='none')
plt.show()

