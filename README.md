Sparse NMF
----------

Example script for Sparse NMF with beta-divergence distortion function and L1 penalty on the activations.

# Disclaimer
This port has not been fully tested, especially with parameters `beta_div != 1`.

# Copyright notice and License
Copyright (C) 2015 Mitsubishi Electric Research Labs 
Jonathan Le Roux, Felix Weninger, John R. Hershey

Code licensed under the Apache 2.0 license  (http://www.apache.org/licenses/LICENSE-2.0) 

# Citation
If you use this code, please cite:
> J. Le Roux, J. R. Hershey, F. Weninger, 
> "Sparse NMF -- half-baked or well done?," 
>  MERL Technical Report, TR2015-023, March 2015
```{tex}
@TechRep{LeRoux2015mar,
	author = {{Le Roux}, J. and Hershey, J. R. and Weninger, F.},
	title = {Sparse {NMF} -- half-baked or well done?},
	institution = {Mitsubishi Electric Research Labs (MERL)},
	number = {TR2015-023},
     address = {Cambridge, MA, USA},
     month = mar,
     year = 2015
}
```

Python version by Maxime W., Jul. 2016

# Usage
tbw.
